export POSTGRES_EXPORT_PORT=5433:
export SENTINEL_EXPORT_PORT=26380:

dev-run: dev-docker-up swagger
	@echo ">> Starting API with SwaggerUI at http://localhost:8080/swagger/docs/"
	go run internal/cmd/http/http.go

dev-full-run: dev-prepare dev-run

dev-docker-up:
	docker-compose up -d appdb sentinel

dev-docker-down:
	docker-compose down

dev-prepare:
	./tools/prepare_dev.sh

dev-test: dev-docker-up
	./docker/Application/bin/run_test.sh

docker-run:
	docker-compose up

docker-test:
	docker-compose up -d;docker-compose exec app /usr/local/bin/app/run_test.sh

docker-build:
	docker-compose build

glide-update:
	glide update --force

glide-install:
	glide install

migrate-create:
	go run ./internal/cmd/migrate/migrate.go create $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))

migrate-apply:
	go run ./internal/cmd/migrate/migrate.go apply

migrate-down:
	go run ./internal/cmd/migrate/migrate.go down

seed:
	go run ./internal/cmd/seeder/seeder.go

swagger:
	go-swagger-gen spec -m -b returns/internal/cmd/http -o ./build/swagger.json --compact
