returns
===========

# Docker Setup for local development/testing

## Requirements

### Install docker, docker-compose

    brew update
    brew install docker docker-compose

### Build & Run

    docker-compose build
    docker-compose up
    docker-compose down (required to down all services and networks)
