FROM longsnapmart/golang:1.12

ENV TZ=Asia/Manila
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN set -e; \
    go get github.com/go-playground/overalls; \
    go get github.com/smacker/go-swagger-gen

ENV GO111MODULE=on

WORKDIR /go/src/bitbucket.org/snapmartinc/returns

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY ./docker/Application/bin /usr/local/bin/app

ARG IS_DEV_MODE

RUN chmod +x /usr/local/bin/app/* \
    && if [ -n "$IS_DEV_MODE" ]; then /usr/local/bin/app/install_dev.sh; fi

COPY ./ /go/src/bitbucket.org/snapmartinc/returns

RUN set -ex; \
    go build -o ./build/http internal/cmd/http/http.go \
    && go build -o ./build/migrate internal/cmd/migrate/migrate.go \
    && go build -o ./build/seeder internal/cmd/seeder/seeder.go

RUN go mod vendor
ENV GO111MODULE=off
RUN set -ex \
    && go-swagger-gen spec -m -b bitbucket.org/snapmartinc/returns/internal/cmd/http  -o ./build/swagger.json --compact
ENV GO111MODULE=on

CMD /usr/local/bin/app/http.sh
