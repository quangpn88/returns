package queue

import "errors"

var ErrorInValidJobStruct = errors.New("invalid job struct")
