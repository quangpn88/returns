package mock

import "strconv"

type GeneratorMock struct {
	count int64
}

func NewGeneratorMock() *GeneratorMock {
	return &GeneratorMock{}
}

func (g *GeneratorMock) Generate(seqKey string, prefix string, length int) (string, error) {
	g.count += 1
	return strconv.FormatInt(g.count, 10), nil
}

func (g *GeneratorMock) GenerateWithStartAt(seqKey string, prefix string, length, startAt int) (string, error) {
	return "", nil
}
