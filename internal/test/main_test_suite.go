package test

import (
	"database/sql"
	"errors"
	"fmt"
	"os"

	"bitbucket.org/snapmartinc/logger"
	"bitbucket.org/snapmartinc/returns/internal/cmd/migrate/migrations"
	"bitbucket.org/snapmartinc/returns/internal/config"
	"bitbucket.org/snapmartinc/returns/internal/connection"
	"github.com/alicebob/miniredis"
	"github.com/jinzhu/gorm"
	"github.com/khaiql/dbcleaner/engine"
	"github.com/pressly/goose"
	"github.com/stretchr/testify/suite"
	"gopkg.in/khaiql/dbcleaner.v2"
	"gopkg.in/redis.v5"
	"gopkg.in/testfixtures.v2"
)

type MainTestSuite struct {
	suite.Suite
	AppConfig     config.AppConfig
	DbTest        *gorm.DB
	DbCleaner     dbcleaner.DbCleaner
	Fixtures      *testfixtures.Context
	redisTest     *redis.Client
	loggerFactory logger.Factory
}

// Get current DB config
func (s *MainTestSuite) getDBConf() connection.PostgresConfig {
	conf := connection.PostgresConfig{
		Host:      s.AppConfig.DbHost,
		Port:      s.AppConfig.DbPort,
		DbName:    s.AppConfig.DbName,
		User:      s.AppConfig.DbUser,
		Pass:      s.AppConfig.DbPass,
		LogEnable: s.AppConfig.DbLogEnable,
	}

	return conf
}

// This will run before the test/s in the suite are executed
func (s *MainTestSuite) SetupSuite() {
	s.AppConfig = config.GetAppConfigFromEnv()

	// Set Database
	s.setDb()

	// remove default callback for CreatedAt UpdatedAt
	// we do it in our models only when needed
	//s.DbTest.Callback().Create().Remove("gorm:update_time_stamp")
	//s.DbTest.Callback().Update().Remove("gorm:update_time_stamp")

	// Redis
	r, err := miniredis.Run()
	s.PanicOnError(err)
	s.redisTest = redis.NewClient(&redis.Options{Addr: r.Addr()})

	// Logger
	s.loggerFactory = logger.NewLoggerFactory(logger.DebugLevel)
}

func (s *MainTestSuite) setDb() {
	dbConf := s.getDBConf()
	var dbTestName string

	sqlDb, err := sql.Open("postgres", dbConf.GetGormPostgresUrl())
	s.PanicOnError(err)

	// Create a DB for testing
	dbTestName = dbConf.DbName + "_test"
	checkStatement := fmt.Sprintf(`SELECT EXISTS(SELECT datname FROM pg_catalog.pg_database WHERE datname = '%s');`, dbTestName)
	row := sqlDb.QueryRow(checkStatement)
	var exists bool
	err = row.Scan(&exists)

	if exists == false {
		_, err = sqlDb.Exec("CREATE DATABASE " + dbTestName + " OWNER " + dbConf.User)
		s.PanicOnError(err)
	}
	err = sqlDb.Close()
	s.PanicOnError(err)

	// Open new DB connection using test DB
	dbConf.DbName = dbTestName
	s.DbTest, err = gorm.Open("postgres", dbConf.GetGormPostgresUrl())
	if err != nil {
		panic(err)
	}

	s.DbTest.LogMode(false)

	// From this point, the test database is now initialized
	// Run migrations
	migrations.SetDB(s.DbTest)
	migrationPath, err := s.GetMigrationPath()
	err = goose.Run("up", s.DbTest.DB(), migrationPath)
	s.PanicOnError(err)

	// Run fixtures
	s.Fixtures, err = testfixtures.NewFolder(s.DbTest.DB(), &testfixtures.PostgreSQL{}, "../../cmd/seeder/fixtures")
	if err != nil {
		s.PanicOnError(err)
	}

	if err := s.Fixtures.Load(); err != nil {
		s.PanicOnError(err)
	}

	// Initialize DB Cleaner
	s.DbCleaner = dbcleaner.New()
	dbPostgres := engine.NewPostgresEngine(dbConf.GetGormPostgresUrl())
	s.DbCleaner.SetEngine(dbPostgres)
}

func (s *MainTestSuite) PanicOnError(err error) {
	if err != nil {
		panic(err)
	}
}

func (s *MainTestSuite) GetMigrationPath() (string, error) {
	var migrationPaths = []string{
		"../cmd/migrate/migrations",
		"../../cmd/migrate/migrations",
	}

	for _, path := range migrationPaths {
		if exist, err := folderExists(path); exist == true {
			return path, err
		}
	}

	return "", errors.New("Cannot find migration folder")
}

// Check if folder exists
func folderExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}

	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
