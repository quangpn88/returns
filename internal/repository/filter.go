package repository

const (
	IgnoreLimit  = -1
	IgnoreOffset = -1
)

type Where map[string][]interface{}
type Filter interface {
	GetWhere() Where
	GetLimit() int
	GetOffset() int
	GetOrderBy() []string
}
