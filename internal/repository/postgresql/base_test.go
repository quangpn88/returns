package postgresql_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"

	"bitbucket.org/snapmartinc/returns/internal/handler/filter"
	"bitbucket.org/snapmartinc/returns/internal/repository/postgresql"
	"bitbucket.org/snapmartinc/returns/internal/util/test"
)

type BaseRepoTestSuite struct {
	test.BehaviourTestSuite
}

type TmpModel struct {
	Id     string
	Field1 string
	Field2 string

	ChildTmpModel ChildTmpModel `json:"warehouseContacts" gorm:"foreignkey:TmpModelId"`
}

type ChildTmpModel struct {
	Id         string
	TmpModelId string
	Field1     string
}

func (m *TmpModel) GetID() string {
	return m.Id
}

func (suite *BaseRepoTestSuite) SetupTest() {
	suite.BehaviourTestSuite.SetupTest()

	err := suite.DB().CreateTable(&TmpModel{}).Error
	suite.PanicOnError(err)
}

func (suite *BaseRepoTestSuite) TestFindById() {
	suite.DB().Create(&TmpModel{Id: "1111"})
	suite.DB().Create(&TmpModel{Id: "2222"})

	repo := postgresql.NewBaseRepo(suite.DB())
	var result TmpModel
	err := repo.FindByID(context.Background(), &result, "2222")

	suite.Nil(err)
	suite.Equal(result.Id, "2222")
}

func (suite *BaseRepoTestSuite) TestSearch() {
	suite.DB().Create(&TmpModel{Id: "111", Field1: "aaa", Field2: "bbb"})
	suite.DB().Create(&TmpModel{Id: "222", Field1: "aaa", Field2: "bbb"})
	suite.DB().Create(&TmpModel{Id: "333", Field1: "bbb", Field2: "ccc"})

	repo := postgresql.NewBaseRepo(suite.DB())
	result := make([]TmpModel, 3)
	f := filter.NewPaginationFilter()
	f.AddWhere("", "field1 = ?", "aaa").AddWhere("", "field2 = ?", "bbb")
	err := repo.Search(context.Background(), &result, f)

	suite.Nil(err)
	suite.Len(result, 2)
	suite.Equal("111", result[0].Id)
	suite.Equal("222", result[1].Id)
}

func (suite *BaseRepoTestSuite) TestSearchHavingChild() {
	err := suite.DB().CreateTable(&ChildTmpModel{}).Error
	suite.PanicOnError(err)

	suite.DB().Create(&TmpModel{Id: "111", Field1: "aaa", Field2: "bbb"})
	suite.DB().Create(&ChildTmpModel{Id: "222", Field1: "aaa", TmpModelId: "111"})

	repo := postgresql.NewBaseRepo(suite.DB())
	result := make([]TmpModel, 3)
	f := filter.NewPaginationFilter()
	f.AddWhere("", "field1 = ?", "aaa")
	err = repo.Search(context.Background(), &result, f, "ChildTmpModel")

	suite.Nil(err)
	suite.Len(result, 1)
	suite.Equal("111", result[0].Id)
	suite.Equal("222", result[0].ChildTmpModel.Id)
}

func (suite *BaseRepoTestSuite) TestCreateOrUpdate() {
	suite.DB().Create(&TmpModel{Id: "111", Field1: "aaa", Field2: "aaa"})
	suite.DB().Create(&TmpModel{Id: "222", Field1: "bbb", Field2: "bbb"})

	repo := postgresql.NewBaseRepo(suite.DB())

	suite.T().Run("Update", func(t *testing.T) {
		var m TmpModel
		repo.CreateOrUpdate(context.Background(), &m, TmpModel{Id: "111"}, TmpModel{Field1: "ccc", Field2: "ccc"})
		suite.DB().Where("id = ?", "111").Find(&m)
		suite.Equal("ccc", m.Field1)
		suite.Equal("ccc", m.Field2)
	})
	suite.T().Run("Create", func(t *testing.T) {
		var m TmpModel
		repo.CreateOrUpdate(context.Background(), &m, TmpModel{Id: "333"}, TmpModel{Field1: "ddd", Field2: "ddd"})
		suite.DB().Where("id = ?", "333").Find(&m)
		suite.Equal("ddd", m.Field1)
		suite.Equal("ddd", m.Field2)
	})
	suite.T().Run("No change", func(t *testing.T) {
		var result TmpModel
		suite.DB().Where("id = ?", "222").Find(&result)
		suite.Equal("bbb", result.Field1)
		suite.Equal("bbb", result.Field2)
	})
}

func (suite *BaseRepoTestSuite) TestUpdate() {
	m := TmpModel{Id: "111", Field1: "aaa", Field2: "bbb"}
	suite.DB().Create(&m)

	repo := postgresql.NewBaseRepo(suite.DB())
	err := repo.Update(context.Background(), &m, TmpModel{Field1: "ccc", Field2: "ddd"})
	suite.Nil(err)

	var updatedRecord TmpModel
	suite.DB().Where("id = ?", "111").Find(&updatedRecord)
	suite.Equal("ccc", updatedRecord.Field1)
	suite.Equal("ddd", updatedRecord.Field2)
}

func (suite *BaseRepoTestSuite) TestCreate() {
	repo := postgresql.NewBaseRepo(suite.DB())

	m := TmpModel{Id: "111", Field1: "aaa", Field2: "bbb"}
	err := repo.Create(context.Background(), &m)
	suite.Nil(err)

	var newRecord TmpModel
	suite.DB().Where("id = ?", "111").Find(&newRecord)
	suite.Equal("aaa", newRecord.Field1)
	suite.Equal("bbb", newRecord.Field2)
}

func TestBaseRepoRunner(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, new(BaseRepoTestSuite))
}
