package postgresql

import (
	"bitbucket.org/snapmartinc/returns/internal/model"
	"bitbucket.org/snapmartinc/returns/internal/repository"
	"context"
	"github.com/jinzhu/gorm"
)

type returnRepo struct {
	*BaseRepo
}

func NewReturnRepo(db *gorm.DB) returnRepo {
	return returnRepo{NewBaseRepo(db)}
}

func (repo returnRepo) GetReturnDetail(ctx context.Context, id string) (*model.Return, error) {
	var res model.Return
	err := repo.getDB(ctx).Preload("Items").Where("id = ?", id).Take(&res).Error
	if err != nil && gorm.IsRecordNotFoundError(err) {
		return nil, repository.RecordNotFound
	}
	return &res, err
}
