package postgresql

import "github.com/jinzhu/gorm"

type returnItemRepo struct {
	*BaseRepo
}

func NewReturnItemRepo(db *gorm.DB) returnItemRepo {
	return returnItemRepo{NewBaseRepo(db)}
}
