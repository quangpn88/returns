package postgresql

import (
	"bitbucket.org/snapmartinc/returns/internal/util/transaction"
	"context"
	"github.com/jinzhu/gorm"

	"bitbucket.org/snapmartinc/returns/internal/model"
	"bitbucket.org/snapmartinc/returns/internal/repository"
)

type BaseRepo struct {
	db *gorm.DB
}

func NewBaseRepo(db *gorm.DB) *BaseRepo {
	return &BaseRepo{
		db: db,
	}
}

func (r *BaseRepo) FindByID(ctx context.Context, m model.Model, id string) error {
	err := r.getDB(ctx).Where("id = ?", id).Take(m).Error
	if err == gorm.ErrRecordNotFound {
		return repository.RecordNotFound
	}
	return err
}

func (r *BaseRepo) CreateOrUpdate(ctx context.Context, m model.Model, query interface{}, attrs ...interface{}) error {
	return r.getDB(ctx).Where(query).Assign(attrs...).FirstOrCreate(m).Error
}

func (r *BaseRepo) Update(ctx context.Context, m model.Model, attrs ...interface{}) error {
	return r.getDB(ctx).Model(m).Update(attrs...).Error
}

func (r *BaseRepo) Create(ctx context.Context, m model.Model) error {
	return r.getDB(ctx).Create(m).Error
}

func (r *BaseRepo) Save(ctx context.Context, m model.Model) error {
	return r.getDB(ctx).Model(m).Save(m).Error
}

func (r *BaseRepo) Search(ctx context.Context, val interface{}, f repository.Filter, preloadFields ...string) error {
	q := r.getDB(ctx).Model(val)
	for query, val := range f.GetWhere() {
		q = q.Where(query, val...)
	}

	if f.GetLimit() > 0 {
		q = q.Limit(f.GetLimit())
	}

	for _, p := range preloadFields {
		q = q.Preload(p)
	}
	if len(f.GetOrderBy()) > 0 {
		for _, order := range f.GetOrderBy() {
			q = q.Order(order)
		}
	}

	return q.Offset(f.GetOffset()).Find(val).Error
}

func (r *BaseRepo) getDB(ctx context.Context) *gorm.DB {
	if tnx := transaction.GetTnx(ctx); tnx != nil {
		return tnx.(*gorm.DB)
	}
	return r.db
}
