package repository

import (
	"bitbucket.org/snapmartinc/returns/internal/model"
	"context"

	"github.com/pkg/errors"
)

var RecordNotFound = errors.New("record not found")

type BaseRepo interface {
	Searchable
	Updatable
	Creatable
	Saveable
	CanFindByID
	CanCreateOrUpdate
}

type Searchable interface {
	Search(ctx context.Context, val interface{}, f Filter, preloadFields ...string) error
}

type Updatable interface {
	Update(ctx context.Context, m model.Model, attrs ...interface{}) error
}

type Creatable interface {
	Create(ctx context.Context, m model.Model) error
}

type Saveable interface {
	Save(ctx context.Context, m model.Model) error
}

type CanFindByID interface {
	FindByID(ctx context.Context, m model.Model, id string) error
}

type CanCreateOrUpdate interface {
	CreateOrUpdate(ctx context.Context, m model.Model, query interface{}, attrs ...interface{}) error
}

type ReturnRepo interface {
	BaseRepo
	GetReturnDetail(ctx context.Context, id string) (*model.Return, error)
}

type ReturnItemRepo interface {
	BaseRepo
}
