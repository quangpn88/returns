package seeders

import (
	"bitbucket.org/snapmartinc/returns/internal/config"
	"bitbucket.org/snapmartinc/returns/internal/model"
	"bitbucket.org/snapmartinc/returns/internal/util/uuid_generator"
	"fmt"
	"github.com/jinzhu/gorm"
	"strconv"
)

type Return struct {
	*Table
}

func NewReturn(db *gorm.DB) *Return {
	return &Return{NewTable(db)}
}

func (r *Return) ShouldRun() bool {
	if r.Count("returns") > 0 {
		return false
	}
	return true
}

func (r *Return) Run() error {
	for i := 0; i < 100; i++ {
		if err := r.Create(r.createSeed(i)); err != nil {
			return err
		}
	}
	return nil
}

func (r *Return) createSeed(i int) *model.Return {
	return &model.Return{
		BaseModel:              model.BaseModel{ID: uuid_generator.NewUUIDV4()},
		PlatformName:           randomStringFromSlice(config.PlatformNames),
		PackageID:              uuid_generator.NewUUIDV4(),
		FulfillmentOrderNumber: strconv.FormatInt(int64(i+5), 10),
		Reason:                 fmt.Sprintf("test %v", i),
		CreatedBy:              uuid_generator.NewUUIDV4(),
	}
}
