package seeders

import (
	"bitbucket.org/snapmartinc/returns/internal/model"
	"bitbucket.org/snapmartinc/returns/internal/util/uuid_generator"
	"fmt"
	"github.com/jinzhu/gorm"
	"math/rand"
)

type ReturnItem struct {
	*Table
	returns []model.Return
}

func NewReturnItem(db *gorm.DB) *ReturnItem {
	return &ReturnItem{Table: NewTable(db)}
}

func (r *ReturnItem) ShouldRun() bool {
	if r.Count("return_items") > 0 {
		return false
	}
	r.db.Find(&r.returns)
	return true
}

func (r *ReturnItem) Run() error {

	for _, re := range r.returns {
		count := rand.Intn(10)
		for n := 0; n < count; n++ {
			if err := r.Create(r.createSeed(re, n)); err != nil {
				return err
			}
		}
	}
	return nil
}

func (r *ReturnItem) createSeed(re model.Return, i int) *model.ReturnItem {
	return &model.ReturnItem{
		BaseModel:        model.BaseModel{ID: uuid_generator.NewUUIDV4()},
		ReturnID:         re.ID,
		PackageItemID:    uuid_generator.NewUUIDV4(),
		Name:             fmt.Sprintf("Name %v", i),
		ProductURL:       fmt.Sprintf("url %v", i),
		SKU:              fmt.Sprintf("sku %v", i),
		ReturnedQuantity: int64(rand.Intn(10)),
		RefundAmount:     float64(rand.Intn(20)),
	}
}
