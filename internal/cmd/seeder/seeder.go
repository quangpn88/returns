package main

import (
	"bitbucket.org/snapmartinc/returns/internal/cmd/seeder/seeders"
	"fmt"

	"bitbucket.org/snapmartinc/returns/internal/config"
	"bitbucket.org/snapmartinc/returns/internal/connection"
)

func main() {
	appConf := config.GetAppConfigFromEnv()
	postgresConfig := connection.PostgresConfig{
		Host:      appConf.DbHost,
		Port:      appConf.DbPort,
		DbName:    appConf.DbName,
		User:      appConf.DbUser,
		Pass:      appConf.DbPass,
		LogEnable: appConf.DbLogEnable,
	}

	gDB := connection.CreateDB(postgresConfig)
	gDB.LogMode(true)
	gDB.DB().SetMaxOpenConns(1)

	runner([]Seeder{
		seeders.NewReturn(gDB),
		seeders.NewReturnItem(gDB),
	})
}

type Seeder interface {
	ShouldRun() bool
	Prepare() error
	Run() error
	Commit() error
	Rollback() error
}

func runner(list []Seeder) {
	for _, s := range list {
		// Check if the current seeder should run
		if !s.ShouldRun() {
			// Skip the current seeder
			continue
		}

		fmt.Printf("seeding %T\n", s)
		if err := s.Prepare(); err != nil {
			panic(err)
		}
		if err := s.Run(); err != nil {
			if err := s.Rollback(); err != nil {
				fmt.Println("Rollback failed", err)
			}
			panic(err)
		}
		if err := s.Commit(); err != nil {
			fmt.Println("Commit failed", err)
		}
	}
}
