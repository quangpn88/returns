package main

import (
	"bitbucket.org/snapmartinc/logger"
	"bitbucket.org/snapmartinc/returns/internal/config"
	"bitbucket.org/snapmartinc/returns/internal/connection"
	"bitbucket.org/snapmartinc/returns/internal/handler"
	"bitbucket.org/snapmartinc/returns/internal/repository/postgresql"
	internalRouter "bitbucket.org/snapmartinc/returns/internal/router"
	internalMiddleware "bitbucket.org/snapmartinc/returns/internal/router/middleware"
	"bitbucket.org/snapmartinc/returns/internal/service"
	"bitbucket.org/snapmartinc/returns/internal/util/env"
	"bitbucket.org/snapmartinc/returns/internal/util/response"
	"bitbucket.org/snapmartinc/returns/internal/util/transaction"
	"bitbucket.org/snapmartinc/returns/internal/util/validation"
	"bitbucket.org/snapmartinc/router"
	"context"
	"errors"
	"fmt"
	"github.com/go-chi/chi"
	chiMiddleware "github.com/go-chi/chi/middleware"
	"github.com/lazada/swgui"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

func notFound(rw http.ResponseWriter, _ *http.Request) {
	res := response.ErrorResponse(errors.New("404 Not Found"), 404)
	response.RenderJson(rw, res)
}

func main() {
	appConf := config.GetAppConfigFromEnv()

	// Logger factory
	var loggerFactory logger.Factory

	if appConf.RunMode == "dev" {
		loggerFactory = logger.NewLoggerFactory(logger.DebugLevel)
	} else {
		loggerFactory = logger.NewLoggerFactory(logger.InfoLevel)
	}

	// init DB Connection
	postgresConfig := connection.PostgresConfig{
		Host:                  appConf.DbHost,
		Port:                  appConf.DbPort,
		DbName:                appConf.DbName,
		User:                  appConf.DbUser,
		Pass:                  appConf.DbPass,
		LogEnable:             appConf.DbLogEnable,
		MaxIdleConnection:     appConf.MaxIdleConnection,
		MaxConnectionLifetime: appConf.MaxConnectionLifetime,
	}
	db := connection.CreateDB(postgresConfig)

	// init NewRelic Application
	newRelicConf := connection.NewRelicConfig{
		AppName: appConf.NewRelicAppName,
		License: appConf.NewRelicLicense,
	}
	newRelicApp := connection.CreateNewRelicApp(newRelicConf)

	// Validation
	validator := validation.NewValidator()

	// Env
	envFactory := func(ctx context.Context) *env.Env {
		return env.NewEnv(&appConf, db, ctx, validator, loggerFactory)
	}

	// Router
	routerConfig := router.Configuration{
		LoggerFactory: loggerFactory,
		NewrelicApp:   newRelicApp,
	}
	routerConfig.AccessLog.Disable = appConf.DisableAccesslog

	r, err := router.New(routerConfig)

	if err != nil {
		logger.Emergency(err)

		panic(err)
	}

	r.Group(func(r chi.Router) {
		r.Get("/swagger/api-docs", internalRouter.SwaggerHandler)
		r.Mount("/swagger/docs/", swgui.NewHandlerWithConfig(
			swgui.Handler{
				Title:       "API UI",
				SwaggerJSON: "/swagger/api-docs",
				BasePath:    "/swagger/docs/",

				JsonEditor: true, // Enable visual json editor support
			},
		))

		r.Mount("/metrics", promhttp.Handler())
		r.Mount("/debug", chiMiddleware.Profiler())
		r.With(internalMiddleware.Env(envFactory)).Get("/health-check", internalMiddleware.MakeHandler(handler.HealthCheckGet))
		r.NotFound(notFound)
	})
	internalValidator := validation.NewInternalValidator(validator)
	tnxManager := transaction.NewTxManager(db)
	returnRepo := postgresql.NewReturnRepo(db)
	returnItemRepo := postgresql.NewReturnItemRepo(db)

	returnService := service.NewReturn(
		internalValidator,
		returnRepo,
		returnItemRepo,
		tnxManager,
	)

	handlerFuncs := internalRouter.HandlerFuncs{
		ReturnList: internalMiddleware.MakeHandler(handler.NewReturnList(returnRepo).Handle),
		ReturnPost: internalMiddleware.MakeHandler(handler.NewReturnPost(returnService).Handle),
		ReturnGet:  internalMiddleware.MakeHandler(handler.NewReturnGet(returnRepo).Handle),
	}

	r.Group(func(r chi.Router) {
		r.Use(internalMiddleware.Env(envFactory))
		api := internalRouter.Create(handlerFuncs)
		r.Mount("/api", api)
	})

	logger.Info("Start listening...")
	if err := http.ListenAndServe(fmt.Sprintf(":%v", appConf.HttpPort), r); err != nil {
		logger.Emergency(err)

		panic(err)
	}
}
