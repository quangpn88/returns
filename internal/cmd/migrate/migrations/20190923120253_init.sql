-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE returns
(
    id                       uuid                     NOT NULL PRIMARY KEY,
    platform_name            VARCHAR(255)             NOT NULL,
    package_id               uuid                     NOT NULL,
    tracking_number          VARCHAR(255)             NOT NULL,
    fulfillment_order_number varchar(255)             NOT NULL,
    reason                   varchar(255),
    created_by               uuid                     NOT NULL,
    created_at               timestamp with time zone NOT NULL,
    updated_at               timestamp with time zone NOT NULL
);

CREATE INDEX returns_platform_name_idx ON returns (platform_name);
CREATE INDEX returns_created_at_idx ON returns (created_at);
CREATE INDEX returns_updated_at_idx ON returns (updated_at);
CREATE UNIQUE INDEX returns_package_id_unique_idx ON returns (package_id);
CREATE INDEX returns_fulfillment_order_number_idx ON returns (fulfillment_order_number);

create table return_items
(
    id                uuid                     NOT NULL PRIMARY KEY,
    return_id         uuid                     NOT NULL,
    package_item_id   uuid                     NOT NULL,
    name              varchar(255)             NOT NULL,
    product_url       varchar(255)             NOT NULL,
    sku               varchar(255)             NOT NULL,
    returned_quantity INTEGER                  NOT NULL,
    refund_amount     FLOAT                    NOT NULL,
    created_at        timestamp with time zone NOT NULL,
    updated_at        timestamp with time zone NOT NULL
);

CREATE INDEX returns_items_return_id_idx ON return_items (return_id);

-- +goose Down
-- +goose DisableTransaction
-- SQL section 'Down' is executed when this migration is rolled back
-- +goose StatementBegin
DO
$$
    DECLARE
        r RECORD;
    BEGIN
        FOR r IN (SELECT tablename
                  FROM pg_tables
                  WHERE schemaname = current_schema()
                    AND tablename NOT IN ('goose_db_version', 'spatial_ref_sys'))
            LOOP
                EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
            END LOOP;
    END
$$;
-- +goose StatementEnd
