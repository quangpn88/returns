package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
	"github.com/pressly/goose"

	"bitbucket.org/snapmartinc/returns/internal/cmd/migrate/migrations"
	"bitbucket.org/snapmartinc/returns/internal/config"
	"bitbucket.org/snapmartinc/returns/internal/connection"
)

var (
	flags = flag.NewFlagSet("goose", flag.ExitOnError)
	dir   = flags.String("dir", "internal/cmd/migrate/migrations", "directory with migration files")
)

func main() {
	flags.Usage = usage
	flags.Parse(os.Args[1:])

	args := flags.Args()
	if len(args) < 1 {
		flags.Usage()
		return
	}

	command := args[0]

	if command == "-h" || command == "--help" {
		flags.Usage()
		return
	}

	appConf := config.GetAppConfigFromEnv()
	dbConfig := connection.PostgresConfig{
		Host:      appConf.DbHost,
		Port:      appConf.DbPort,
		DbName:    appConf.DbName,
		User:      appConf.DbUser,
		Pass:      appConf.DbPass,
		LogEnable: appConf.DbLogEnable,
	}
	gDB := connection.CreateDB(dbConfig)
	gDB.LogMode(false)

	dialect := gDB.Dialect().GetName()

	switch dialect {
	case "postgres", "mysql", "sqlite3":
		if err := goose.SetDialect(dialect); err != nil {
			log.Fatal(err)
		}
	default:
		log.Fatalf("%q driver not supported\n", dialect)
	}

	migrations.SetDB(gDB)

	if err := goose.Run(command, gDB.DB(), *dir, args[1:]...); err != nil {
		log.Fatalf("goose run: %v", err)
	}
}

func usage() {
	fmt.Print(usagePrefix)
	flags.PrintDefaults()
	fmt.Print(usageCommands)
	fmt.Print(usageCreate)
}

var (
	usagePrefix = `Usage: migrate [OPTIONS] COMMAND

Options:
`

	usageCommands = `
Commands:
    up                   Migrate the DB to the most recent version available
    up-by-one            Migrate the DB up by 1
    up-to VERSION        Migrate the DB to a specific VERSION
    down                 Roll back the version by 1
    down-to VERSION      Roll back to a specific VERSION
    redo                 Re-run the latest migration
    reset                Roll back all migrations
    status               Dump the migration status for the current DB
    version              Print the current version of the database
    create NAME [sql|go] Creates new migration file with the current timestamp
    fix                  Apply sequential ordering to migrations
`

	usageCreate = `create must be of form: migrate [OPTIONS] create NAME [go|sql|gorm]`
)
