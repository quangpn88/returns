package config

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type UserService struct {
	BaseUrl    string        `envconfig:"BASE_URL" required:"true"`
	Timeout    time.Duration `envconfig:"TIMEOUT" required:"true"`
	MaxAttempt int           `envconfig:"MAX_ATTEMPT" required:"true"`
	WaitTime   int           `envconfig:"WAIT_TIME" required:"true"` // Millisecond
	CacheTime  time.Duration `envconfig:"CACHE_TIME" required:"true"`
}

type AppConfig struct {
	Consumer         Consumer
	RunMode          string `envconfig:"RUN_MODE" required:"true"`
	HttpPort         int    `envconfig:"HTTP_PORT" required:"true"`
	DisableAccesslog bool   `envconfig:"DISABLE_ACCESS_LOG" required:"false"`

	NewRelicAppName string `envconfig:"NEWRELIC_APPNAME" required:"true"`
	NewRelicLicense string `envconfig:"NEWRELIC_LICENSE" required:"true"`

	DbHost                string        `envconfig:"POSTGRES_HOST" required:"true"`
	DbPort                int           `envconfig:"POSTGRES_PORT" required:"true"`
	DbName                string        `envconfig:"POSTGRES_DB" required:"true"`
	DbUser                string        `envconfig:"POSTGRES_USER" required:"true"`
	DbPass                string        `envconfig:"POSTGRES_PASS" required:"true"`
	DbLogEnable           bool          `envconfig:"POSTGRES_LOG_MODE_ENABLE"`
	DbMaxConnection       int           `envconfig:"POSTGRES_MAX_CONNECTION" required:"true"`
	MaxIdleConnection     int           `envconfig:"POSTGRES_MAX_IDLE_CONNECTION" required:"true"`
	MaxConnectionLifetime time.Duration `envconfig:"POSTGRES_MAX_CONNECTION_LIFETIME" required:"true"`

	RedisMaster string `envconfig:"REDIS_MASTER" required:"true"`
	SetinelHost string `envconfig:"SENTINEL_HOST" required:"true"`
	SetinelPort string `envconfig:"SENTINEL_PORT" required:"true"`
}

type Consumer struct {
	PollDuration    time.Duration `envconfig:"CONSUMER_POLL_DURATION" required:"true"`
	DefaultReplicas int           `envconfig:"CONSUMER_DEFAULT_REPLICAS" required:"true"`
}

func GetAppConfigFromEnv() AppConfig {
	var conf AppConfig
	envconfig.MustProcess("", &conf)
	return conf
}
