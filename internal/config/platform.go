package config

const (
	PlatformLanders  = "LANDERS"
	PlatformSnapMart = "SNAPMART"
)

var (
	PlatformNames = []string{
		PlatformLanders,
		PlatformSnapMart,
	}
)
