package handler

import (
	"bitbucket.org/snapmartinc/returns/internal/repository"
	"bitbucket.org/snapmartinc/returns/internal/util/response"
	"net/http"
)

type returnGet struct {
	returnRepo repository.ReturnRepo
}

func NewReturnGet(returnRepo repository.ReturnRepo) returnGet {
	return returnGet{returnRepo: returnRepo}
}

// swagger:route GET /returns/customer-returns/{id} Returns CustomerReturnGet
//
// Get CustomerReturn
//
// Parameters:
// id		string     in:path true "Customer Return ID"
//
// Responses:
// 200: "OK"
// | schema:
// |   properties:
// |     data:
// |       $ref: "#/definitions/Return"
// 400: "Bad Request"
// | schema:
// |   properties:
// |     errors:
// |       $ref: "#/definitions/ErrorResponse"
// 500: "Internal Server Error"
// | schema:
// |   properties:
// |     errors:
// |       $ref: "#/definitions/ErrorResponse"
func (r returnGet) Handle(ctx Context) response.ApiResponse {
	id := ctx.URLParam("id")
	returnModel, err := r.returnRepo.GetReturnDetail(ctx.Context(), id)
	if err == repository.RecordNotFound {
		return response.ErrorResponse(err, http.StatusNotFound)
	}
	if err != nil {
		return response.ErrorResponse(err, http.StatusInternalServerError)
	}
	return response.Ok(returnModel)
}
