package handler

import (
	"net/http"

	"bitbucket.org/snapmartinc/returns/internal/util/response"
)

func HealthCheckGet(ctx Context) response.ApiResponse {
	status := "Error"
	statusCode := http.StatusInternalServerError
	if ctx.DB().DB().Ping() == nil {
		status = "OK"
		statusCode = http.StatusOK
	}
	return response.ApiResponse{
		Code: statusCode,
		Data: map[string]string{"status": status},
	}
}
