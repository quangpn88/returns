package handler

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/gorilla/schema"
	"github.com/jinzhu/gorm"

	"bitbucket.org/snapmartinc/returns/internal/repository"
	"bitbucket.org/snapmartinc/returns/internal/repository/postgresql"
	"bitbucket.org/snapmartinc/returns/internal/util/env"
	"bitbucket.org/snapmartinc/returns/internal/util/response"
)

var schemaDecoder = schema.NewDecoder()

type Context struct {
	e *env.Env
	w http.ResponseWriter
	r *http.Request
}

func NewContext(e *env.Env, w http.ResponseWriter, r *http.Request) Context {
	return Context{e, w, r}
}

func (c *Context) Context() context.Context {
	return c.r.Context()
}

// DB Get db from ENV
func (c *Context) DB() *gorm.DB {
	return c.e.GetDB()
}

// BaseRepo Returns base repository
func (c *Context) BaseRepo() repository.BaseRepo {
	return postgresql.NewBaseRepo(c.DB())
}

// URLParam Get Url param
func (c *Context) URLParam(key string) string {
	return chi.URLParam(c.r, key)
}

// DecodeURLParam Parses url params to target
func (c *Context) DecodeURLParam(target interface{}) error {
	return schemaDecoder.Decode(target, c.r.URL.Query())
}

// DecodePayload Decode payload to target
func (c *Context) DecodePayload(target interface{}) error {
	decoder := json.NewDecoder(c.r.Body)
	return decoder.Decode(target)
}

// Validate Shortcuts to validate a struct
func (c *Context) Validate(s interface{}) error {
	return c.e.GetValidator().Struct(s)
}

// PanicOnError
func (c *Context) PanicOnError(err error) {
	if err != nil {
		panic(err)
	}
}

// Func Handler function
type HandlerFunc func(ctx Context) response.ApiResponse
