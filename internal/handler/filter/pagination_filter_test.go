package filter_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/snapmartinc/returns/internal/handler/filter"
)

func TestPaginationFilter_LimitOffset(t *testing.T) {
	f := filter.NewPaginationFilter()
	a := assert.New(t)

	t.Run("Default", func(sub *testing.T) {
		f.Page = 0
		f.PerPage = 0
		a.Equal(0, f.GetOffset())
		a.Equal(50, f.GetLimit())
	})

	t.Run("Custom Values", func(sub *testing.T) {
		f.Page = 2
		f.PerPage = 5
		a.Equal(5, f.GetOffset())
		a.Equal(5, f.GetLimit())
	})

	t.Run("Page 1", func(sub *testing.T) {
		f.Page = 1
		f.PerPage = 5
		a.Equal(0, f.GetOffset())
		a.Equal(5, f.GetLimit())
	})

	t.Run("Page 2", func(sub *testing.T) {
		f.Page = 2
		f.PerPage = 5
		a.Equal(5, f.GetOffset())
		a.Equal(5, f.GetLimit())
	})
}
