package filter

import "bitbucket.org/snapmartinc/returns/internal/repository"

type BasicFilter struct {
	where repository.Where
	keys  map[string]bool
}

func NewBasicFilter() *BasicFilter {
	return &BasicFilter{
		where: repository.Where{},
		keys:  map[string]bool{},
	}
}

// implement repository.Filter interface
func (f *BasicFilter) GetLimit() int {
	return 0
}

// implement repository.Filter interface
func (f *BasicFilter) GetOffset() int {
	return 0
}

// implement repository.Filter interface
func (f *BasicFilter) GetWhere() repository.Where {
	return f.where
}

func (f *BasicFilter) AddWhere(key string, query string, values ...interface{}) *BasicFilter {
	f.where[query] = values
	f.keys[key] = true
	return f
}
