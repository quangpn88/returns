package filter_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/snapmartinc/returns/internal/handler/filter"
	"bitbucket.org/snapmartinc/returns/internal/repository"
)

func TestBasicFilter_AddWhere(t *testing.T) {
	f := filter.NewBasicFilter()
	f.AddWhere("key1", "field1 = ?", 1)
	f.AddWhere("key2", "field2 = ?", 2)

	a := assert.New(t)
	a.Equal(
		repository.Where{
			"field1 = ?": []interface{}{1},
			"field2 = ?": []interface{}{2},
		},
		f.GetWhere(),
	)
}

func TestBasicFilter_GetLimitOffset(t *testing.T) {
	f := filter.NewBasicFilter()
	a := assert.New(t)
	a.Equal(0, f.GetOffset())
	a.Equal(0, f.GetLimit())
}
