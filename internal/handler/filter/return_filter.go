package filter

// swagger:model
type returnFilter struct {
	*PaginationFilter
}

func NewReturnFilter() returnFilter {
	return returnFilter{PaginationFilter: NewPaginationFilter()}
}
