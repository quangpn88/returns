package filter

const (
	defaultPerPage = 50
	maxPerPage     = 200
)

// swagger:parameters paginationFilters
type PaginationFilter struct {
	BasicFilter
	BasicOrder

	// Page Number - Default is 1
	Page int `json:"page" schema:"page"`

	// Number of items per page - Default: 50, Max: 200
	PerPage int `json:"per_page" schema:"per_page"`

	IgnorePerPage bool `json:"ignore_per_page" schema:"ignore_per_page"`
}

func NewPaginationFilter() *PaginationFilter {
	return &PaginationFilter{
		BasicFilter: *NewBasicFilter(),
		BasicOrder:  *NewBasicOrder(),
	}
}

// implement repository.Filter interface
func (f *PaginationFilter) GetLimit() int {
	return f.GetPerPage()
}

// implement repository.Filter interface
func (f *PaginationFilter) GetOffset() int {
	return (f.GetPage() - 1) * f.GetPerPage()
}

func (f *PaginationFilter) GetPage() int {
	if f.Page < 1 {
		return 1
	}
	return f.Page
}

func (f *PaginationFilter) GetPerPage() int {
	if f.PerPage < 1 || (f.PerPage > maxPerPage && !f.IgnorePerPage) {
		return defaultPerPage
	}

	return f.PerPage
}
