package handler_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/suite"

	"bitbucket.org/snapmartinc/returns/internal/util/test"
)

type HealthCheckTestSuite struct {
	test.BehaviourTestSuite
}

func (suite *HealthCheckTestSuite) TestGet() {
	apiTest := suite.ApiTest(test.AdminUser)
	suite.T().Run("Update example", func(t *testing.T) {
		res := apiTest.Get("/health-check")
		suite.Equal(http.StatusOK, res.Result().StatusCode)
	})
}

func TestHealthCheckRunner(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, new(HealthCheckTestSuite))
}
