// returns API
//
// returns Restful API
//
// Schemes: http, https
// Host: returns.local
// BasePath: /api
// Version: 1.0.0
//
// Security:
//   jwt_auth:
//
// SecurityDefinitions:
//   jwt_auth:
//     type: apiKey
//     name: Authorization
//     description: Authenticate by JWT token.
//     in: header
//
// Consumes:
// - application/json
//
// Produces:
// - application/json
//
// swagger:meta
package handler
