package handler

import (
	"bitbucket.org/snapmartinc/returns/internal/handler/filter"
	"bitbucket.org/snapmartinc/returns/internal/model"
	"bitbucket.org/snapmartinc/returns/internal/repository"
	"bitbucket.org/snapmartinc/returns/internal/util/response"
	"net/http"
)

type ReturnList struct {
	returnRepo repository.ReturnRepo
}

func NewReturnList(returnRepo repository.ReturnRepo) ReturnList {
	return ReturnList{returnRepo: returnRepo}
}

// swagger:route GET /returns/customer-returns Returns ReturnFilter
//
// Get Customer Return collection
//
// Responses:
// 200: "OK"
// | schema:
// |   properties:
// |     data:
// |       type: array
// |       items:
// |         $ref: "#/definitions/Return"
// 400: "Bad Request"
// | schema:
// |   properties:
// |     errors:
// |       $ref: "#/definitions/ErrorResponse"
// 422: "Unprocessable Entity"
// | schema:
// |   properties:
// |     errors:
// |       $ref: "#/definitions/ErrorResponse"
func (r ReturnList) Handle(ctx Context) response.ApiResponse {
	f := filter.NewReturnFilter()
	if err := ctx.DecodeURLParam(&f); err != nil {
		return response.BadRequest(err)
	}
	var returns []model.Return
	err := r.returnRepo.Search(ctx.Context(), &returns, f)
	if err != nil {
		return response.ErrorResponse(err, http.StatusInternalServerError)
	}

	return response.Ok(returns)
}
