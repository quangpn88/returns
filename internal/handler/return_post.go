package handler

import (
	"bitbucket.org/snapmartinc/returns/internal/service"
	"bitbucket.org/snapmartinc/returns/internal/util/response"
)

type returnPost struct {
	returnService service.Return
}

func NewReturnPost(returnService service.Return) returnPost {
	return returnPost{returnService: returnService}
}

// swagger:route POST /returns/customer-returns Returns CustomerReturnPost
//
// Create Customer Return
//
// Parameters:
// content ReturnCreateInput in:body required "Customer Return put form"
//
// Responses:
// 202: "Accepted"
// 400: "Bad Request"
// | schema:
// |   properties:
// |     errors:
// |       $ref: "#/definitions/ErrorResponse"
// 404: "Not Found"
// | schema:
// |   properties:
// |     errors:
// |       $ref: "#/definitions/ErrorResponse"
// 422: "Unprocessable Entity"
// | schema:
// |   properties:
// |     errors:
// |       $ref: "#/definitions/ErrorResponse"
// 500: "Internal Server Error"
// | schema:
// |   properties:
// |     errors:
// |       $ref: "#/definitions/ErrorResponse"
func (r returnPost) Handle(ctx Context) response.ApiResponse {
	var returnCreateInput service.ReturnCreateInput
	if err := ctx.DecodePayload(&returnCreateInput); err != nil {
		return response.BadRequest(err)
	}

	id, err := r.returnService.Create(ctx.Context(), returnCreateInput)
	if err != nil {
		return response.ConvertServiceError(err)
	}
	return response.Accepted(id)
}
