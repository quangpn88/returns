package router

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/snapmartinc/logger"
	"github.com/go-chi/chi"
)

type HandlerFuncs struct {
	ReturnList http.HandlerFunc
	ReturnPost http.HandlerFunc
	ReturnGet  http.HandlerFunc
}

// Create handlers
func Create(handlerFuncs HandlerFuncs) http.Handler {
	api := chi.NewRouter()

	api.Group(func(r chi.Router) {
		r.Route("/returns/customer-returns", func(r chi.Router) {
			r.Get("/", handlerFuncs.ReturnList)
			r.Post("/", handlerFuncs.ReturnPost)
			r.Get("/{id}", handlerFuncs.ReturnGet)
		})
	})
	return api
}

func SwaggerHandler(rw http.ResponseWriter, _ *http.Request) {
	content, err := ioutil.ReadFile("build/swagger.json")
	if err != nil {
		logger.Error(err)
		rw.WriteHeader(500)
		return
	}

	baseURL := os.Getenv("API_BASE_URL")
	if baseURL != "" {
		host := strings.Split(baseURL, "//")[1]
		content = bytes.Replace(content, []byte("returns.local"), []byte(host), 1)
	}

	length, err := rw.Write(content)
	if err != nil {
		logger.Error(err)
		rw.WriteHeader(500)
		return
	}
	rw.Header().Add("Content-Type", "application/json")
	rw.Header().Add("Content-Length", string(length))
	rw.Header().Add("Cache-Control", "no-cache")
}
