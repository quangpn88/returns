package middleware

import (
	"net/http"

	"bitbucket.org/snapmartinc/returns/internal/handler"
	"bitbucket.org/snapmartinc/returns/internal/util/response"
)

// MakeHandler Create handler
func MakeHandler(handlerFunc handler.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		e := getEnvFromCtx(r.Context())
		ctx := handler.NewContext(e, w, r)
		res := handlerFunc(ctx)
		response.RenderJson(w, res)
	}
}
