package connection

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
	"github.com/newrelic/go-agent"
)

type NewRelicConfig struct {
	AppName string
	License string
}

func CreateNewRelicApp(conf NewRelicConfig) newrelic.Application {
	newRelicConf := newrelic.NewConfig(
		conf.AppName,
		conf.License,
	)
	newRelicApp, err := newrelic.NewApplication(newRelicConf)
	if err != nil {
		panic(err)
	}
	return newRelicApp
}
