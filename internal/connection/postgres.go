package connection

import (
	"fmt"
	"time"

	"bitbucket.org/snapmartinc/newrelic-context/nrgorm"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
)

type PostgresConfig struct {
	Host                  string
	Port                  int
	DbName                string
	User                  string
	Pass                  string
	LogEnable             bool
	MaxConnection         int
	MaxIdleConnection     int
	MaxConnectionLifetime time.Duration
}

func (p PostgresConfig) GetGormPostgresUrl() string {
	return fmt.Sprintf(
		"host=%s user=%s port=%d dbname=%s sslmode=disable password=%s",
		p.Host,
		p.User,
		p.Port,
		p.DbName,
		p.Pass,
	)
}

func CreateDB(conf PostgresConfig) *gorm.DB {
	c, err := gorm.Open("postgres", conf.GetGormPostgresUrl())
	if err != nil {
		panic(err)
	}
	c.LogMode(conf.LogEnable)

	// remove default callback for CreatedAt UpdatedAt
	// we do it in our models only when needed
	//c.Callback().Create().Remove("gorm:update_time_stamp")
	//c.Callback().Update().Remove("gorm:update_time_stamp")

	//c.DB().SetMaxOpenConns(conf.MaxConnection)
	//c.DB().SetMaxIdleConns(conf.MaxIdleConnection)
	c.DB().SetConnMaxLifetime(conf.MaxConnectionLifetime)

	nrgorm.AddGormCallbacks(c)

	return c
}
