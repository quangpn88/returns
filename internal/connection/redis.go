package connection

import (
	"time"

	"github.com/go-redis/redis"
)

type RedisConnectionConfig struct {
	RedisMaster string
	SetinelHost string
	SetinelPort string
}

func (r RedisConnectionConfig) GetSentinelAddress() string {
	return r.SetinelHost + ":" + r.SetinelPort
}

func CreateRedisConn(conf RedisConnectionConfig) *redis.Client {
	options := redis.Options{
		Addr:               conf.GetSentinelAddress(),
		MaxRetries:         10,
		DialTimeout:        time.Minute,
		ReadTimeout:        time.Minute,
		WriteTimeout:       time.Minute,
		PoolSize:           1000,
		PoolTimeout:        time.Minute,
		IdleTimeout:        time.Minute,
		IdleCheckFrequency: time.Second * 10,
	}
	return redis.NewClient(&options)
}
