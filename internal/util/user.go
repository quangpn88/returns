package util

import (
	userclient "bitbucket.org/snapmartinc/user-service-client"
	"context"
)

func GetCurrentUserIdFromContext(ctx context.Context) string {
	currentUser := userclient.GetCurrentUserFromContext(ctx)
	if currentUser != nil {
		return currentUser.Id
	}

	return "e8a526ae-d5e8-48f4-9a25-a3045af40206"
}
