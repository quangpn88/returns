package pointer

func ToString(text *string) string {
	if text == nil {
		return ""
	}
	return *text
}
func ToStringPointer(text string) *string {
	return &text
}

func ToIntPointer(num int64) *int64 {
	return &num
}

func ToFloatPointer(num float64) *float64 {
	return &num
}
