package response

import (
	"errors"
	"net/http"

	"github.com/jinzhu/gorm"

	"bitbucket.org/snapmartinc/returns/internal/util/validation"
)

type ApiResponse struct {
	Code    int               `json:"-"`
	Data    interface{}       `json:"data,omitempty"`
	Headers map[string]string `json:"-"`
	Errors  interface{}       `json:"errors,omitempty"`
	Total   *int64            `json:"total,omitempty"`
}

// swagger:model ErrorResponse
type Error struct {
	Message string              `json:"message,omitempty"`
	Code    int                 `json:"code,omitempty"`
	Errors  map[string][]string `json:"errors,omitempty"`
}

// Ok 200-OK with data in body
func Ok(data interface{}) ApiResponse {
	return ApiResponse{http.StatusOK, data, nil, nil, nil}
}

// Accepted 202-Accepted with data in body
func Accepted(data interface{}) ApiResponse {
	return ApiResponse{http.StatusAccepted, data, nil, nil, nil}
}

// Created 201-Created with data in body
func Created(data interface{}) ApiResponse {
	return ApiResponse{http.StatusCreated, data, nil, nil, nil}
}

// PartialContent 206-Partial Content with data in body
func PartialContent(data interface{}) ApiResponse {
	return ApiResponse{http.StatusPartialContent, data, nil, nil, nil}
}

// BadRequest 400-Bad Requestnil
func BadRequest(err error) ApiResponse {
	return ErrorResponse(err, http.StatusBadRequest)
}

// ValidationError 422-Unprocessable Enttity
func ValidationError(err error) ApiResponse {
	errMessages := validation.ParseValidationErr(err)
	return CreateValidationErrResponse(errMessages)
}

func CreateValidationErrResponse(errMessages map[string][]string) ApiResponse {
	var response ApiResponse
	response.Code = http.StatusUnprocessableEntity
	response.Errors = Error{
		Message: http.StatusText(http.StatusUnprocessableEntity),
		Code:    http.StatusUnprocessableEntity,
		Errors:  errMessages,
	}
	return response
}

// ErrorResponse Custom error response
func ErrorResponse(err error, status int) ApiResponse {
	var response ApiResponse
	response.Code = status
	response.Errors = Error{
		Message: err.Error(),
		Code:    status,
	}
	return response
}

func RecordNotFoundError(err error, message string, status int) ApiResponse {
	if err == gorm.ErrRecordNotFound {
		return ErrorResponse(errors.New(message), status)
	}

	return ErrorResponse(err, http.StatusInternalServerError)
}
