package test

import (
	"context"
	"database/sql"
	"net/http"
	"strconv"
	"time"

	"github.com/alicebob/miniredis"
	"github.com/go-chi/chi"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/suite"

	"bitbucket.org/snapmartinc/returns/internal/config"
	"bitbucket.org/snapmartinc/returns/internal/connection"
	"bitbucket.org/snapmartinc/returns/internal/handler"
	"bitbucket.org/snapmartinc/returns/internal/router"
	internalRouter "bitbucket.org/snapmartinc/returns/internal/router/middleware"
	"bitbucket.org/snapmartinc/returns/internal/util/env"
	"bitbucket.org/snapmartinc/returns/internal/util/validation"

	"errors"
	"os"

	"bitbucket.org/snapmartinc/logger"
	"bitbucket.org/snapmartinc/returns/internal/cmd/migrate/migrations"
	"bitbucket.org/snapmartinc/router/middleware"
	userclient "bitbucket.org/snapmartinc/user-service-client"
	"github.com/pressly/goose"
)

type BehaviourTestSuite struct {
	suite.Suite
	AppConf       config.AppConfig
	dbTest        *gorm.DB
	dbTestName    string
	redisTest     *redis.Client
	loggerFactory logger.Factory
}

func (suite *BehaviourTestSuite) getDBConf() connection.PostgresConfig {
	conf := connection.PostgresConfig{
		Host:      suite.AppConf.DbHost,
		Port:      suite.AppConf.DbPort,
		DbName:    suite.AppConf.DbName,
		User:      suite.AppConf.DbUser,
		Pass:      suite.AppConf.DbPass,
		LogEnable: suite.AppConf.DbLogEnable,
	}

	return conf
}

func (suite *BehaviourTestSuite) SetupTest() {
	dbConf := suite.getDBConf()
	sqlDb, err := sql.Open("postgres", dbConf.GetGormPostgresUrl())
	suite.PanicOnError(err)

	suite.dbTestName = dbConf.DbName + "test_" + strconv.Itoa(time.Now().Nanosecond())
	_, err = sqlDb.Exec("DROP DATABASE IF EXISTS " + suite.dbTestName)
	suite.PanicOnError(err)
	_, err = sqlDb.Exec("CREATE DATABASE " + suite.dbTestName + " OWNER " + dbConf.User)
	suite.PanicOnError(err)
	err = sqlDb.Close()
	suite.PanicOnError(err)

	dbConf.DbName = suite.dbTestName
	suite.dbTest, err = gorm.Open("postgres", dbConf.GetGormPostgresUrl())
	if err != nil {
		panic(err)
	}
	suite.dbTest.LogMode(false)

	// remove default callback for CreatedAt UpdatedAt
	// we do it in our models only when needed
	suite.dbTest.Callback().Create().Remove("gorm:update_time_stamp")
	suite.dbTest.Callback().Update().Remove("gorm:update_time_stamp")

	// Redis
	s, err := miniredis.Run()
	suite.PanicOnError(err)
	suite.redisTest = redis.NewClient(&redis.Options{Addr: s.Addr()})

	// Migration
	migrations.SetDB(suite.dbTest)
	migrationPath, err := suite.GetMigrationPath()
	err = goose.Run("up", suite.dbTest.DB(), migrationPath)
	suite.PanicOnError(err)

	// Logger
	suite.loggerFactory = logger.NewLoggerFactory(logger.DebugLevel)
}

func (suite *BehaviourTestSuite) ApiTest(u *userclient.User) *ApiTest {
	userMock := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if u != nil {
				r = r.WithContext(userclient.ContextWithUser(r.Context(), u))
			}
			next.ServeHTTP(w, r)
		})
	}
	validator := validation.NewValidator()
	envFactory := func(ctx context.Context) *env.Env {
		return env.NewEnv(&suite.AppConf, suite.dbTest, ctx, validator, suite.loggerFactory)
	}

	r := chi.NewRouter()
	r.Use(userMock)
	r.Use(middleware.Recoverer(nil))
	r.Use(middleware.ContextLogger(suite.loggerFactory))
	r.Use(internalRouter.Env(envFactory))

	handlerFuncs := router.HandlerFuncs{}
	r.Mount("/api", router.Create(handlerFuncs))
	r.Get("/health-check", internalRouter.MakeHandler(handler.HealthCheckGet))

	return NewApiTest(r)
}

func (suite *BehaviourTestSuite) DB() *gorm.DB {
	return suite.dbTest
}

func (suite *BehaviourTestSuite) Logger() logger.Entry {
	return suite.loggerFactory.Logger(context.Background())
}

func (suite *BehaviourTestSuite) SetupSuite() {
	suite.AppConf = config.GetAppConfigFromEnv()
}

func (suite *BehaviourTestSuite) TearDownTest() {
	err := suite.dbTest.Close()
	suite.PanicOnError(err)

	// Drop test db
	conf := suite.getDBConf()
	sqlDb, err := sql.Open("postgres", conf.GetGormPostgresUrl())
	suite.PanicOnError(err)
	_, err = sqlDb.Exec("DROP DATABASE IF EXISTS " + suite.dbTestName)
	suite.PanicOnError(err)
	err = sqlDb.Close()
	suite.PanicOnError(err)
}

func (suite *BehaviourTestSuite) PanicOnError(err error) {
	if err != nil {
		panic(err)
	}
}

func (suite *BehaviourTestSuite) CreateModels(models ...interface{}) {
	for _, m := range models {
		suite.PanicOnError(suite.DB().Create(m).Error)
	}
}

func (suite *BehaviourTestSuite) GetMigrationPath() (string, error) {
	var migrationPaths = []string{
		"../cmd/migrate/migrations",
		"../../cmd/migrate/migrations",
	}

	for _, path := range migrationPaths {
		if exist, err := folderExists(path); exist == true {
			return path, err
		}
	}

	return "", errors.New("Can not find migration folder.")
}

func folderExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
