package env

import (
	"context"
	"reflect"
	"time"

	"bitbucket.org/snapmartinc/newrelic-context"
	"github.com/go-chi/chi/middleware"
	"github.com/jinzhu/gorm"
	"gopkg.in/go-playground/validator.v9"

	"bitbucket.org/snapmartinc/returns/internal/config"

	"bitbucket.org/snapmartinc/logger"
)

type Env struct {
	conf *config.AppConfig
	db   *gorm.DB
	ctx  context.Context
	v    *validator.Validate
	lf   logger.Factory
}

func NewEnv(conf *config.AppConfig,
	db *gorm.DB,
	ctx context.Context,
	v *validator.Validate,
	lf logger.Factory) *Env {
	if ctx == nil {
		ctx = context.Background()
	}

	db = nrcontext.SetTxnToGorm(ctx, db)

	return &Env{
		conf: conf,
		db:   db,
		ctx:  ctx,
		v:    v,
		lf:   lf,
	}
}

func (e *Env) GetLogger() logger.Entry {
	entry := logger.EntryFromContext(e.ctx)

	if entry == nil {
		entry = e.lf.Logger(e.ctx)

		e.ctx = logger.ContextWithEntry(entry, e.ctx)
	}

	return entry
}

func (e *Env) GetAppConf() *config.AppConfig {
	return e.conf
}

func (e *Env) GetDB() *gorm.DB {
	return e.db
}

func (e *Env) GetValidator() *validator.Validate {
	return e.v
}

func (e *Env) GetContextId() string {
	return middleware.GetReqID(e.ctx)
}

// context.Context interface

func (e *Env) Deadline() (deadline time.Time, ok bool) {
	return e.ctx.Deadline()
}

func (e *Env) Done() <-chan struct{} {
	return e.ctx.Done()
}

func (e *Env) Err() error {
	return e.ctx.Err()
}

func (e *Env) Value(key interface{}) interface{} {
	return e.ctx.Value(key)
}

// Env changers

func WithValue(parent *Env, key, val interface{}) *Env {
	if key == nil {
		panic("nil key")
	}
	if !reflect.TypeOf(key).Comparable() {
		panic("key is not comparable")
	}
	env := *parent
	env.ctx = context.WithValue(env.ctx, key, val)
	return &env
}

func WithContext(parent *Env, ctx context.Context) *Env {
	env := *parent
	env.ctx = ctx
	return &env
}

func MustWithTx(parent *Env) *Env {
	env := *parent
	env.db = mustGetTx(parent.db)
	return &env
}

func mustGetTx(db *gorm.DB) *gorm.DB {
	tx := db.Begin()
	if tx.Error != nil {
		panic(tx.Error)
	}
	return tx
}
