package cache

import "errors"

var (
	Nil = errors.New("cache: key is missing")
)
