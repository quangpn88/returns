package cache

import (
	"time"

	"github.com/go-redis/cache"
	"github.com/go-redis/redis"
)

type Redis struct {
	codec  *cache.Codec
	prefix string
	ttl    time.Duration
}

func NewRedis(c *redis.Client, prefix string, ttl time.Duration) *Redis {
	return &Redis{
		codec:  NewRedisCodec(c),
		prefix: prefix,
		ttl:    ttl,
	}
}

func (r *Redis) Get(key string, obj interface{}) error {
	err := r.codec.Get(r.cacheKey(key), obj)
	if err == cache.ErrCacheMiss {
		return Nil
	}
	return err
}

func (r *Redis) Set(key string, obj interface{}) error {
	return r.codec.Set(&cache.Item{
		Key:        r.cacheKey(key),
		Object:     obj,
		Expiration: r.ttl,
	})
}

func (r *Redis) Delete(key string) error {
	err := r.codec.Delete(r.cacheKey(key))
	if err == cache.ErrCacheMiss {
		return Nil
	}
	return err
}

func (r *Redis) cacheKey(key string) string {
	return r.prefix + "/" + key
}
