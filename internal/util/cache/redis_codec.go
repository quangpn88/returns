package cache

import (
	"encoding/json"
	"github.com/go-redis/redis"

	"github.com/go-redis/cache"
)

func NewRedisCodec(redis *redis.Client) *cache.Codec {
	return &cache.Codec{
		Redis: redis,

		Marshal:   json.Marshal,
		Unmarshal: json.Unmarshal,
	}
}
