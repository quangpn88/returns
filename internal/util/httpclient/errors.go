package httpclient

import (
	"fmt"
	"net/http"
)

func makeClientError(code int) error {
	return fmt.Errorf("Http client error %v", code)
}

func makeServerError(code int) error {
	return fmt.Errorf("Http client error %v", code)
}

var (
	ErrorBadRequest                   = makeClientError(http.StatusBadRequest)
	ErrorUnauthorized                 = makeClientError(http.StatusUnauthorized)
	ErrorPaymentRequired              = makeClientError(http.StatusPaymentRequired)
	ErrorForbidden                    = makeClientError(http.StatusForbidden)
	ErrorNotFound                     = makeClientError(http.StatusNotFound)
	ErrorMethodNotAllowed             = makeClientError(http.StatusMethodNotAllowed)
	ErrorNotAcceptable                = makeClientError(http.StatusNotAcceptable)
	ErrorProxyAuthRequired            = makeClientError(http.StatusProxyAuthRequired)
	ErrorRequestTimeout               = makeClientError(http.StatusRequestTimeout)
	ErrorConflict                     = makeClientError(http.StatusConflict)
	ErrorGone                         = makeClientError(http.StatusGone)
	ErrorLengthRequired               = makeClientError(http.StatusLengthRequired)
	ErrorPreconditionFailed           = makeClientError(http.StatusPreconditionFailed)
	ErrorRequestEntityTooLarge        = makeClientError(http.StatusRequestEntityTooLarge)
	ErrorRequestURITooLong            = makeClientError(http.StatusRequestURITooLong)
	ErrorUnsupportedMediaType         = makeClientError(http.StatusUnsupportedMediaType)
	ErrorRequestedRangeNotSatisfiable = makeClientError(http.StatusRequestedRangeNotSatisfiable)
	ErrorExpectationFailed            = makeClientError(http.StatusExpectationFailed)
	ErrorTeapot                       = makeClientError(http.StatusTeapot)
	ErrorUnprocessableEntity          = makeClientError(http.StatusUnprocessableEntity)
	ErrorLocked                       = makeClientError(http.StatusLocked)
	ErrorFailedDependency             = makeClientError(http.StatusFailedDependency)
	ErrorUpgradeRequired              = makeClientError(http.StatusUpgradeRequired)
	ErrorPreconditionRequired         = makeClientError(http.StatusPreconditionRequired)
	ErrorTooManyRequests              = makeClientError(http.StatusTooManyRequests)
	ErrorRequestHeaderFieldsTooLarge  = makeClientError(http.StatusRequestHeaderFieldsTooLarge)
	ErrorUnavailableForLegalReasons   = makeClientError(http.StatusUnavailableForLegalReasons)

	ErrorInternalServerError           = makeServerError(http.StatusInternalServerError)
	ErrorNotImplemented                = makeServerError(http.StatusNotImplemented)
	ErrorBadGateway                    = makeServerError(http.StatusBadGateway)
	ErrorServiceUnavailable            = makeServerError(http.StatusServiceUnavailable)
	ErrorGatewayTimeout                = makeServerError(http.StatusGatewayTimeout)
	ErrorHTTPVersionNotSupported       = makeServerError(http.StatusHTTPVersionNotSupported)
	ErrorVariantAlsoNegotiates         = makeServerError(http.StatusVariantAlsoNegotiates)
	ErrorInsufficientStorage           = makeServerError(http.StatusInsufficientStorage)
	ErrorLoopDetected                  = makeServerError(http.StatusLoopDetected)
	ErrorNotExtended                   = makeServerError(http.StatusNotExtended)
	ErrorNetworkAuthenticationRequired = makeServerError(http.StatusNetworkAuthenticationRequired)
)

var errorsMap = map[int]error{
	http.StatusBadRequest:                   ErrorBadRequest,
	http.StatusUnauthorized:                 ErrorUnauthorized,
	http.StatusPaymentRequired:              ErrorPaymentRequired,
	http.StatusForbidden:                    ErrorForbidden,
	http.StatusNotFound:                     ErrorNotFound,
	http.StatusMethodNotAllowed:             ErrorMethodNotAllowed,
	http.StatusNotAcceptable:                ErrorNotAcceptable,
	http.StatusProxyAuthRequired:            ErrorProxyAuthRequired,
	http.StatusRequestTimeout:               ErrorRequestTimeout,
	http.StatusConflict:                     ErrorConflict,
	http.StatusGone:                         ErrorGone,
	http.StatusLengthRequired:               ErrorLengthRequired,
	http.StatusPreconditionFailed:           ErrorPreconditionFailed,
	http.StatusRequestEntityTooLarge:        ErrorRequestEntityTooLarge,
	http.StatusRequestURITooLong:            ErrorRequestURITooLong,
	http.StatusUnsupportedMediaType:         ErrorUnsupportedMediaType,
	http.StatusRequestedRangeNotSatisfiable: ErrorRequestedRangeNotSatisfiable,
	http.StatusExpectationFailed:            ErrorExpectationFailed,
	http.StatusTeapot:                       ErrorTeapot,
	http.StatusUnprocessableEntity:          ErrorUnprocessableEntity,
	http.StatusLocked:                       ErrorLocked,
	http.StatusFailedDependency:             ErrorFailedDependency,
	http.StatusUpgradeRequired:              ErrorUpgradeRequired,
	http.StatusPreconditionRequired:         ErrorPreconditionRequired,
	http.StatusTooManyRequests:              ErrorTooManyRequests,
	http.StatusRequestHeaderFieldsTooLarge:  ErrorRequestHeaderFieldsTooLarge,
	http.StatusUnavailableForLegalReasons:   ErrorUnavailableForLegalReasons,

	http.StatusInternalServerError:           ErrorInternalServerError,
	http.StatusNotImplemented:                ErrorNotImplemented,
	http.StatusBadGateway:                    ErrorBadGateway,
	http.StatusServiceUnavailable:            ErrorServiceUnavailable,
	http.StatusGatewayTimeout:                ErrorGatewayTimeout,
	http.StatusHTTPVersionNotSupported:       ErrorHTTPVersionNotSupported,
	http.StatusVariantAlsoNegotiates:         ErrorVariantAlsoNegotiates,
	http.StatusInsufficientStorage:           ErrorInsufficientStorage,
	http.StatusLoopDetected:                  ErrorLoopDetected,
	http.StatusNotExtended:                   ErrorNotExtended,
	http.StatusNetworkAuthenticationRequired: ErrorNetworkAuthenticationRequired,
}
