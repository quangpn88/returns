package service

import (
	"bitbucket.org/snapmartinc/returns/internal/repository"
	"bitbucket.org/snapmartinc/returns/internal/util"
	"bitbucket.org/snapmartinc/returns/internal/util/transaction"
	"bitbucket.org/snapmartinc/returns/internal/util/validation"
	"context"
	"github.com/pkg/errors"
)

type Return interface {
	Create(ctx context.Context, input ReturnCreateInput) (id string, err error)
}

type returnService struct {
	validator      validation.Validator
	returnRepo     repository.ReturnRepo
	returnItemRepo repository.ReturnItemRepo
	tnxManager     transaction.TnxManager
}

func NewReturn(
	validator validation.Validator,
	returnRepo repository.ReturnRepo,
	returnItemRepo repository.ReturnItemRepo,
	tnxManager transaction.TnxManager,
) returnService {
	return returnService{
		validator:      validator,
		returnRepo:     returnRepo,
		returnItemRepo: returnItemRepo,
		tnxManager:     tnxManager,
	}
}

func (r returnService) Create(ctx context.Context, input ReturnCreateInput) (id string, err error) {
	if err := r.validator.Validate(input); err != nil {
		return "", NewValidationError(err)
	}
	userID := util.GetCurrentUserIdFromContext(ctx)
	returnModel := input.ConvertToModel(userID)
	tnx, ctx := r.tnxManager.Start(ctx)
	defer func() {
		err = tnx.Finish(err)
	}()
	if err := r.returnRepo.Create(ctx, &returnModel); err != nil {
		return "", errors.Wrap(err, "cannot create return")
	}
	for i := range input.Items {
		itemModel := input.Items[i].ConvertToModel(returnModel.ID)
		if err := r.returnItemRepo.Create(ctx, &itemModel); err != nil {
			return "", errors.Wrap(err, "cannot create item")
		}
	}
	return returnModel.ID, nil
}
