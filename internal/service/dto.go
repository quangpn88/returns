package service

import "bitbucket.org/snapmartinc/returns/internal/model"

// swagger:model
type ReturnCreateInput struct {
	PlatformName           string                  `json:"platformName" valid:"required"`
	PackageID              string                  `json:"packageId" valid:"required"`
	TrackingNumber         string                  `json:"trackingNumber" valid:"required"`
	FulfillmentOrderNumber string                  `json:"fulfillmentOrderNumber" valid:"required"`
	Reason                 string                  `json:"reason" valid:"required"`
	Items                  []ReturnItemCreateInput `json:"items" valid:"min=1,dive"`
}

func (r ReturnCreateInput) ConvertToModel(userID string) model.Return {
	return model.Return{
		PlatformName:           r.PlatformName,
		PackageID:              r.PackageID,
		TrackingNumber:         r.TrackingNumber,
		FulfillmentOrderNumber: r.FulfillmentOrderNumber,
		Reason:                 r.Reason,
		CreatedBy:              userID,
	}
}

type ReturnItemCreateInput struct {
	PackageItemID    string  `json:"packageItemId" valid:"required"`
	Name             string  `json:"name" valid:"required"`
	ProductURL       string  `json:"productUrl" valid:"required"`
	SKU              string  `json:"sku" valid:"required"`
	ReturnedQuantity int64   `json:"returnedQuantity" valid:"required"`
	RefundAmount     float64 `json:"refundAmount" valid:"required"`
}

func (r ReturnItemCreateInput) ConvertToModel(returnID string) model.ReturnItem {
	return model.ReturnItem{
		ReturnID:         returnID,
		PackageItemID:    r.PackageItemID,
		Name:             r.Name,
		ProductURL:       r.ProductURL,
		SKU:              r.SKU,
		ReturnedQuantity: r.ReturnedQuantity,
		RefundAmount:     r.RefundAmount,
	}
}
