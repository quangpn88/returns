package model

// swagger:model
type ReturnItem struct {
	BaseModel
	ReturnID         string  `json:"returnId"`
	PackageItemID    string  `json:"packageItemId"`
	Name             string  `json:"name"`
	ProductURL       string  `json:"productUrl"`
	SKU              string  `json:"sku"`
	ReturnedQuantity int64   `json:"returnedQuantity"`
	RefundAmount     float64 `json:"refundAmount"`
}
