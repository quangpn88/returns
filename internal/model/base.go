package model

import (
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	"time"
)

type Model interface {
	GetID() string
}

type BaseModel struct {
	ID        string    `gorm:"type:uuid;primary_key" json:"id"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

func (m *BaseModel) GetID() string {
	return m.ID
}

// BeforeCreate This function is called before Create()
func (m *BaseModel) BeforeCreate(scope *gorm.Scope) error {
	uuid4, err := uuid.NewV4()
	if err != nil {
		return err
	}

	if m.ID == "" {
		err := scope.SetColumn("ID", uuid4.String())
		if err != nil {
			return err
		}
	}

	now := time.Now()
	now = now.Round(time.Second)
	// Set created_at column
	if m.CreatedAt.IsZero() {
		err := scope.SetColumn("created_at", now)
		if err != nil {
			return err
		}
	}

	// Set updated_at column
	if m.UpdatedAt.IsZero() {
		err := scope.SetColumn("updated_at", now)
		if err != nil {
			return err
		}
	}

	return nil
}

func (m *BaseModel) BeforeUpdate(scope *gorm.Scope) error {
	now := time.Now()
	now = now.Round(time.Second)
	_ = scope.SetColumn("updated_at", now)
	return nil
}

func (m *BaseModel) RevisionKey() string {
	return m.ID
}
