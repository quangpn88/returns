package model

// swagger:model
type Return struct {
	BaseModel
	PlatformName           string       `json:"platformName"`
	PackageID              string       `json:"packageId"`
	TrackingNumber         string       `json:"trackingNumber"`
	FulfillmentOrderNumber string       `json:"fulfillmentOrderNumber"`
	Reason                 string       `json:"reason"`
	CreatedBy              string       `json:"createdBy"`
	Items                  []ReturnItem `json:"items"`
}
