package model

import (
	"github.com/jinzhu/gorm"
	"time"
)

type UpdatedModel struct {
	UpdatedAt time.Time `json:"updated_at"`
}

// This function is called before Create
func (m *UpdatedModel) BeforeCreate(scope *gorm.Scope) error {
	now := time.Now()
	now = now.Round(time.Second)

	// Set updated_at column
	if m.UpdatedAt.IsZero() {
		err := scope.SetColumn("UpdatedAt", now)
		if err != nil {
			return err
		}
	}
	return nil
}

// This function is called before Update
func (m *UpdatedModel) BeforeUpdate(scope *gorm.Scope) error {
	now := time.Now()
	now = now.Round(time.Second)
	// Set updated_at column
	err := scope.SetColumn("UpdatedAt", now)
	if err != nil {
		return err
	}
	return nil
}
