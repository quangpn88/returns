package model

import (
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
)

type DeletedModel struct {
	DeletedAt pq.NullTime `json:"deleted_at"`
}

// This function is called before Create
func (m *DeletedModel) BeforeCreate(scope *gorm.Scope) error {
	// Set deleted_at column
	err := scope.SetColumn("DeletedAt", nil)
	if err != nil {
		return err
	}
	return nil
}
