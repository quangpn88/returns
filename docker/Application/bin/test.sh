#!/usr/bin/env bash
glide install
set -e

apt-get update
apt-get install -y --force-yes bc

/usr/local/bin/app/run_test.sh