#!/usr/bin/env bash
cd $GOPATH/src/bitbucket.org/snapmartinc/returns
set -e
mkdir -p build

COVERAGE_AIM=5

overalls -ignore .glide,vendor,build -project=bitbucket.org/snapmartinc/returns
COVER=$(go tool cover -func=overalls.coverprofile | tail -n 1 | awk '{ print $3 }' | tr -d %)
mkdir -p build
go tool cover -html=overalls.coverprofile -o build/coverage.html
echo "Coverage: $COVER%"
if (( $(echo "$COVERAGE_AIM > $COVER" |bc -l) )); then
    echo "Coverage test failed. Current coverage is $COVER% but should be $COVERAGE_AIM%";
    exit 1
fi;
FILES=$(gofmt -l $(ls -d */ | grep -Ev 'vendor|env|bin|conf'))
[[ $FILES = "" ]] || echo "gofmt required for ${FILES}"
exit $([[ $FILES = "" ]] && echo 0 || echo 1)
