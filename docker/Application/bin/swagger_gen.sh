#!/usr/bin/env bash
set -e
go mod vendor
echo ">>>> GO MODULE off"
export GO111MODULE=off
echo ">>>> gen swagger"
go-swagger-gen spec -m -b bitbucket.org/snapmartinc/returns/internal/cmd/http  -o ./build/swagger.json --compact
echo ">>>> validating swagger.json"
go-swagger-gen validate build/swagger.json
echo ">>>> GO MODULE on"
export GO111MODULE=on