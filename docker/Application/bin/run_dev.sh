#!/usr/bin/env bash
set -e

cd /go/src/bitbucket.org/snapmartinc/returns

export GO111MODULE=on

echo ">> Precompile dependencies"
go install "bitbucket.org/snapmartinc/returns/internal/cmd/http"

echo '>> Build migrations...'
go build -o ./build/migrate internal/cmd/migrate/migrate.go
echo '>> Run migrations...'
./build/migrate up

echo '>> Build seeder...'
go build -o ./build/seeder internal/cmd/seeder/seeder.go
# Dev has to seed data manually
echo '>> Run seeder...'
./build/seeder

#echo '>> Build swagger.json...'
#/usr/local/bin/app/swagger_gen.sh

/usr/local/bin/app/reload.sh "bitbucket.org/snapmartinc/returns/internal/cmd/http" "/tmp/http"

echo "running"
