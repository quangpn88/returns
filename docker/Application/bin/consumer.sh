#!/usr/bin/env bash
set -e

cd /go/src/bitbucket.org/snapmartinc/returns

export GO111MODULE=on

echo ">> Precompile dependencies"
go install "bitbucket.org/snapmartinc/returns/internal/cmd/consumer"

/usr/local/bin/app/reload.sh "bitbucket.org/snapmartinc/returns/internal/cmd/consumer" "/tmp/consumer"