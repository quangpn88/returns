package config

import (
	"io/ioutil"
	"os"
	"strings"
)

func init() {
	mainEnvPath := `./docker/Application/env_files/main.env`
	cwd, _ := os.Getwd()
	dirs := strings.Split(cwd, "/")
	if strings.Contains(cwd, "internal") {
		for dirs[len(dirs)-1] != "internal" {
			mainEnvPath = "../" + mainEnvPath
			dirs = dirs[:len(dirs)-1]
		}
		mainEnvPath = "../" + mainEnvPath
	}

	dockerVars, err := ioutil.ReadFile(mainEnvPath)
	if err != nil {
		panic(`Unable to read ` + mainEnvPath + `: ` + err.Error() + `, cwd: ` + cwd)
	}

	vars := `POSTGRES_HOST=localhost
POSTGRES_PORT=5433
SENTINEL_HOST=localhost
SENTINEL_PORT=26380
` + string(dockerVars)

	for _, tuple := range strings.Split(vars, "\n") {
		tuple = strings.Trim(tuple, " \t\r")
		if tuple == `` || tuple[0] == '#' {
			continue
		}

		tupleItems := strings.Split(tuple, `=`)
		if os.Getenv(tupleItems[0]) == `` {
			os.Setenv(tupleItems[0], tupleItems[1])
		}
	}

}
