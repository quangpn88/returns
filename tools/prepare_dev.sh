#!/usr/bin/env bash
set -e

echo '>> Install dependencies'
if [ ! -x "$(which overalls)" ]; then
    echo 'github.com/go-playground/overalls'
    go get github.com/go-playground/overalls
fi
if [ ! -x "$(which go-swagger-gen)" ]; then
    echo 'github.com/smacker/go-swagger-gen'
    go get github.com/smacker/go-swagger-gen
fi

echo 'vendor'
if [ -d "./vendor" ]; then
    echo "> Don't install as directory ./vendor exists! Remove it and restart docker in case you need it"
else
    glide install
fi

if [ ! -f "./internal/config/dev.go" ]; then
    echo '>> Inject default dev config...'
    cp ./tools/dev.go ./internal/config/dev.go
fi

echo '>> Run migrations...'
go run internal/cmd/migrate/migrate.go apply

# Dev has to seed data manually
##echo '>> Run seeder...'
##go run internal/cmd/seeder/seeder.go

echo '>> Build swagger.json...'
go-swagger-gen spec -m -b returns/internal/cmd/http  -o ./build/swagger.json --compact
