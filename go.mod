module bitbucket.org/snapmartinc/returns

go 1.12

require (
	bitbucket.org/snapmartinc/logger v0.0.0-20190722102907-70e1fed01587
	bitbucket.org/snapmartinc/newrelic-context v0.0.0-20190723030343-e091ae63165b
	bitbucket.org/snapmartinc/router v0.0.0-20190919101135-9d8b2ba6285d
	bitbucket.org/snapmartinc/shippingproviders v0.0.0-20190912091233-0444dd858dcc // indirect
	bitbucket.org/snapmartinc/user-service-client v0.0.0-20190916112339-fefd7d2a4d59
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-openapi/runtime v0.19.6 // indirect
	github.com/go-redis/cache v6.4.0+incompatible
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/schema v1.1.0
	github.com/jinzhu/gorm v1.9.10
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/khaiql/dbcleaner v2.3.0+incompatible
	github.com/lazada/swgui v0.1.1
	github.com/lib/pq v1.2.0
	github.com/newrelic/go-agent v2.12.0+incompatible
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.8.1
	github.com/pressly/goose v2.6.0+incompatible
	github.com/prometheus/client_golang v1.1.0
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20190617133340-57b3e21c3d56 // indirect
	golang.org/x/tools v0.0.0-20190617190820-da514acc4774 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1
	gopkg.in/khaiql/dbcleaner.v2 v2.3.0
	gopkg.in/redis.v5 v5.2.9
	gopkg.in/testfixtures.v2 v2.5.3
)
